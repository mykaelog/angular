import { Config } from './../_models/config'

export const HOME_CONFIGURATION : Config[] = [
    {
        name : 'home_configuration',
        value : 'Home Configuration Value',
        description : 'Description for \'home_configuration\''
    }
];