import { Config } from './../_models/config'

export const CORE_CONFIGURATION : Config[] = [
    {
        name : 'site_name',
        value : 'My First Angular 2 Site!!',
        description : 'Description for \'site_name\''
    }
];