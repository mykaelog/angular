import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { ContainerService } from './_services/container.service';

@Component({
    selector: 'app-component',
    templateUrl: './views/layout/app.component.html',
    styleUrls: ['./app.component.css'],
})


export class AppComponent {
    
    title : string;

    constructor(private _container : ContainerService) {
        this.setProperties();
    }
    
    setProperties() {
        this.title = this._container.getConfig('site_name');
    }

}

