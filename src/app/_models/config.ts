import { Model } from './model';

export class Config extends Model {
    public name : string;
    public value : string;
    public description : string;
}
