import { Model } from './model';


export class Modules extends Model {
    public name : string;
    public description : string;
    public order : number;
}