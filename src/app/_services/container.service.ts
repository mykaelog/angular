import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AutoloaderService } from './autoloader.service';
import { ConfigService } from './config.service';
import { Config } from './../_models/index';


@Injectable()
export class ContainerService  { 

    private _config : ConfigService;

    constructor(private _loader : AutoloaderService) {

        this.loadConfiguration();
    }

    loadConfiguration() {
        this._config = this._loader.loadConfiguration();
        /*this._container.push(
            { "config" : this._loader.loadConfiguration() }
        );*/


    }


    getConfig(name : string) : string {
        
        let element : Config = this._config.getByName(name);
        
        return element.value;
        
        //return this._container['config'][name] || 'null';
    }
    
}

