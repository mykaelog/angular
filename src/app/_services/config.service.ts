import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { environment } from './../../environments/environment';

import { Config } from './../_models/index';
import { CONFIGURATION } from './../_config/index';

@Injectable()
export class ConfigService {
    
    private _urlConfig = '';
    private _configFileName = '_config.json';
    private _data : Config[] = new Array;

    constructor(private _httpService : HttpService) {}

    getAll() : Config[] {
        return this._data;
    }

    getConfigByUrl(value) {
        let data : Array<Config> = new Array<Config>();
        let source : string = '';
        source = source.concat('./app/' + value + '/' + this._configFileName);

        this._httpService.get(source)
            .subscribe(
                (response) =>  this._data.push(response.config), 
                (error) => console.log('ERROR : ' + error),
                () => ((environment.production !== false) ? console.log('Completed fetch from: ' + source, this._data): null)
            );
    }

    getConfigByFile(configuration : Config[]) {
        configuration.forEach((element, index) => {
            return this._data.push(element);
        });
    }

    bootstrap() {

        //By URL working but no efective, can see all configuration from browser
        /*
        CONFIGURATION.forEach((value) => {
            return this.getConfigByUrl(value);
        });
        */

        //By Configuration File
        CONFIGURATION.forEach((element, index) => {
            return this.getConfigByFile(element);
        });

        return this;   
    }

    getByName(name : string) : Config {

        let find : Config;

        this.getAll().forEach((element, index) => {
            if(element.name == name) {
                find = element
            };    
        });

        return find;
    }
}
