import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Config } from './../_models/index';
import { ConfigService } from './config.service';

@Injectable()
export class AutoloaderService {

    constructor(private _configService : ConfigService) {}

    loadConfiguration() : ConfigService {
        return this._configService.bootstrap();
    }

}