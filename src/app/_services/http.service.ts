import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

    private _headers;
    private _options;

    constructor(private http : Http) {}

    get(urlString : string) {
        
        return this.http.get(urlString)
            .map((response : Response) => response.json())
            /*.map((response : Response) => {
                 return response.json()
            })*/
            //.catch(this.handleError);
            //.catch((errors : any) => Observable.throw(errors.json().error || `Server Error`));

    }

    post(urlString : string, body : Object, options : RequestOptions) {
        
        return this.http.post(urlString, body, options)
            .map((response : Response) => response.json())
            .catch(this.handleError);
            //.catch((errors : any) => Observable.throw(errors.json().error || `Server Error`));

    }

    addHeader(type : string, value : string) {
        return { type : value };
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}