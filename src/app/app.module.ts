import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//Services
import { AutoloaderService, ConfigService, HttpService, ContainerService } from './_services/index';

//Main Modules
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

//Shared imports
import { HeaderComponent } from './_shared/index';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        HeaderComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    providers: [
        AutoloaderService, 
        ConfigService,
        HttpService,
        ContainerService
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { 

    //constructor(private loader : AutoloaderService) {}
    /*public _container : Array<any>;

    constructor(private loader : AutoloaderService) {
        this._container = new Array();
        this.loadConfiguration();
    }

    loadConfiguration() {
        this._container.push(
            { "config" : this.loader.loadConfiguration() }
        );

        console.log(this._container);
    }

    getConfig(name : string) {
        return this._container['config'][name] || null;
    }*/
}
