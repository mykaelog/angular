//import { Config } from './../_models/config'

/*export const CONFIGURATION : string[] = [
    'core'
];*/

import { Config } from './../_models/config'
import { CORE_CONFIGURATION } from './../core/_config'
import { HOME_CONFIGURATION } from './../home/_config';

//Add new imports module Key : module_name, Value : Config[]
export const CONFIGURATION : Config[][] = [
    CORE_CONFIGURATION,
    HOME_CONFIGURATION
]